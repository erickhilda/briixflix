export const movies = [
  {
    id: 'b2426b36-4fff-41a8-a382-8cf9f1c15442',
    title: 'Dick Figures: The Movie',
    genres: 'Action|Adventure|Animation',
    director: 'Adela Dyball',
    summary:
      'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.',
    image_url: 'http://dummyimage.com/196x100.png/dddddd/000000',
  },
  {
    id: '36a096c8-cdcb-4c03-a353-6db88ec2e303',
    title: 'Closure',
    genres: 'Documentary',
    director: 'Buddy Dugald',
    summary:
      'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.',
    image_url: 'http://dummyimage.com/188x100.png/ff4444/ffffff',
  },
  {
    id: 'b18e2e44-da81-4b76-b9af-7a506791178b',
    title: 'Aliens in the Attic',
    genres: 'Adventure|Children|Fantasy|Sci-Fi',
    director: 'Winifred Balderstone',
    summary:
      'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.',
    image_url: 'http://dummyimage.com/171x100.png/ff4444/ffffff',
  },
  {
    id: '95e52674-c100-4454-b7d9-8931894ffb04',
    title: 'Silent Night, Deadly Night III: Better Watch Out!',
    genres: 'Horror',
    director: 'Milena Perrinchief',
    summary:
      'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.',
    image_url: 'http://dummyimage.com/204x100.png/cc0000/ffffff',
  },
  {
    id: '8226bf2d-18b6-4ea7-b999-c7d2b1ba4c98',
    title: 'Rocket, The (a.k.a. Maurice Richard)',
    genres: 'Drama',
    director: 'Babs Cutchey',
    summary:
      'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.',
    image_url: 'http://dummyimage.com/147x100.png/ff4444/ffffff',
  },
  {
    id: 'e747a982-c55d-4e4e-91d6-1cff8a605c3f',
    title: 'CSNY Déjà Vu',
    genres: 'Documentary',
    director: 'Kristofer McChesney',
    summary:
      'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.',
    image_url: 'http://dummyimage.com/132x100.png/5fa2dd/ffffff',
  },
  {
    id: '594df128-0061-4326-bc69-2544309ab4c6',
    title: 'Beyond the Forest',
    genres: 'Drama|Film-Noir|Thriller',
    director: 'Herrick Skakunas',
    summary:
      'Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.',
    image_url: 'http://dummyimage.com/179x100.png/dddddd/000000',
  },
  {
    id: '3fdd60cb-fe94-4f4d-bba4-3c169f346eb9',
    title: 'Rocket Singh: Salesman of the Year',
    genres: 'Comedy|Drama',
    director: 'Drusilla Darnell',
    summary:
      'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
    image_url: 'http://dummyimage.com/214x100.png/cc0000/ffffff',
  },
  {
    id: 'b3afabbc-031d-4be3-9391-8860e012ef4b',
    title: 'Kentucky Fried Movie, The',
    genres: 'Comedy',
    director: 'Wallache McLanachan',
    summary: 'Donec semper sapien a libero.',
    image_url: 'http://dummyimage.com/152x100.png/ff4444/ffffff',
  },
  {
    id: '67ad6cde-c8b4-4f01-8df6-795810d1a8f3',
    title: 'Letter from Siberia',
    genres: '(no genres listed)',
    director: 'Cody Waiton',
    summary:
      'Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.',
    image_url: 'http://dummyimage.com/181x100.png/dddddd/000000',
  },
  {
    id: '31d19880-9464-4f26-bf32-0105dfe57a0e',
    title: 'Belly of an Architect, The',
    genres: 'Drama',
    director: 'Layney Element',
    summary:
      'Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.',
    image_url: 'http://dummyimage.com/161x100.png/cc0000/ffffff',
  },
  {
    id: '5d606f9a-6dc0-478b-b606-77c5abc5df66',
    title: 'Lawless',
    genres: 'Crime|Drama',
    director: 'Leoine Moffet',
    summary:
      'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.',
    image_url: 'http://dummyimage.com/150x100.png/dddddd/000000',
  },
  {
    id: '685354c9-267a-4d25-87bc-885da798f5b2',
    title: 'Eva',
    genres: 'Drama|Fantasy|Sci-Fi',
    director: "Rickert O'Shee",
    summary: 'Proin eu mi.',
    image_url: 'http://dummyimage.com/142x100.png/5fa2dd/ffffff',
  },
  {
    id: '1124be0f-f6a1-4178-a9ea-7b04bf837377',
    title: 'Knight Without Armor',
    genres: 'Adventure|Drama|Romance|Thriller',
    director: 'Goldina Blench',
    summary:
      'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam.',
    image_url: 'http://dummyimage.com/246x100.png/ff4444/ffffff',
  },
  {
    id: 'c4911843-d983-414a-a4c4-7e1407131b2e',
    title: 'Beyond the Black Rainbow',
    genres: 'Mystery|Sci-Fi',
    director: 'Salli Tabourin',
    summary:
      'Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.',
    image_url: 'http://dummyimage.com/187x100.png/5fa2dd/ffffff',
  },
  {
    id: '836b398b-56cc-4fbd-9ef7-b59e0661bee3',
    title: 'Summer in Berlin (Sommer vorm Balkon)',
    genres: 'Drama',
    director: 'Winna Rottcher',
    summary:
      'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
    image_url: 'http://dummyimage.com/229x100.png/5fa2dd/ffffff',
  },
  {
    id: '096aa00e-7be9-48a6-8971-a754c7c7161f',
    title: 'Young Ones',
    genres: 'Drama|Sci-Fi|Western',
    director: 'Tobie Sexton',
    summary:
      'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.',
    image_url: 'http://dummyimage.com/176x100.png/dddddd/000000',
  },
  {
    id: '45ac0614-b9e9-4629-b708-0e8751dec60a',
    title: 'Gnome-Mobile, The',
    genres: 'Adventure|Children|Fantasy|Musical',
    director: 'Morganne Maginot',
    summary:
      'In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.',
    image_url: 'http://dummyimage.com/241x100.png/5fa2dd/ffffff',
  },
  {
    id: '6085ddf7-0cc2-4f4e-963c-73debf151d4b',
    title: 'Hearts and Minds',
    genres: 'Documentary|War',
    director: 'Hughie Jovasevic',
    summary:
      'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
    image_url: 'http://dummyimage.com/160x100.png/ff4444/ffffff',
  },
  {
    id: '986f6cf1-f780-4d0b-b3ed-28c26ab0e45a',
    title: 'Big Eden',
    genres: 'Drama|Romance',
    director: 'Iris Weaving',
    summary:
      'Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.',
    image_url: 'http://dummyimage.com/167x100.png/cc0000/ffffff',
  },
  {
    id: '82d25cde-1186-42bf-a9f2-5ab0a898713b',
    title: 'Blacula',
    genres: 'Horror',
    director: 'Silvie Brisseau',
    summary:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.',
    image_url: 'http://dummyimage.com/139x100.png/dddddd/000000',
  },
  {
    id: 'c6898539-4d26-4ad0-84f0-227cc5a351f1',
    title: 'Barabbas',
    genres: 'Drama',
    director: 'Ermengarde Swinnerton',
    summary:
      'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.',
    image_url: 'http://dummyimage.com/113x100.png/cc0000/ffffff',
  },
  {
    id: 'ea4f074e-f2da-4673-8c17-36bc128bf7fe',
    title: 'Rampo (a.k.a. The Mystery of Rampo)',
    genres: 'Drama|Mystery',
    director: 'Tresa Aps',
    summary:
      'Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.',
    image_url: 'http://dummyimage.com/245x100.png/cc0000/ffffff',
  },
  {
    id: '2e68f640-7548-41f8-bd43-cf5fdf68b9c8',
    title: 'Passage to India, A',
    genres: 'Adventure|Drama',
    director: 'Storm Stivers',
    summary:
      'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
    image_url: 'http://dummyimage.com/219x100.png/ff4444/ffffff',
  },
  {
    id: '230d2e5b-bdfd-49ca-8af8-10980e895ed8',
    title: 'Wake Up, Ron Burgundy',
    genres: 'Comedy',
    director: 'Cobby Lammert',
    summary: 'Suspendisse potenti. Nullam porttitor lacus at turpis.',
    image_url: 'http://dummyimage.com/240x100.png/ff4444/ffffff',
  },
  {
    id: 'ee46c7d2-2bc1-4144-b05f-4f9eb5b03b93',
    title: 'Herbie Rides Again',
    genres: 'Adventure|Children|Comedy',
    director: "Gaylord O'Kane",
    summary:
      'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.',
    image_url: 'http://dummyimage.com/211x100.png/ff4444/ffffff',
  },
  {
    id: '47de6e24-900c-4314-8a80-45f4eb3c8c55',
    title: 'Wondrous Oblivion',
    genres: 'Children|Comedy|Drama',
    director: 'Brigida Hanwell',
    summary:
      'Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.',
    image_url: 'http://dummyimage.com/225x100.png/ff4444/ffffff',
  },
  {
    id: 'f1b015e2-049a-4de1-a8bc-39231c4ec7ca',
    title: 'Fiston',
    genres: 'Comedy',
    director: 'Codi Blackledge',
    summary:
      'Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.',
    image_url: 'http://dummyimage.com/178x100.png/dddddd/000000',
  },
  {
    id: '15c9bbba-bb07-4b81-9cc3-5cbf31709760',
    title: 'I Heart Huckabees',
    genres: 'Comedy',
    director: 'Ann-marie Jozefczak',
    summary:
      'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.',
    image_url: 'http://dummyimage.com/176x100.png/dddddd/000000',
  },
  {
    id: '4c115b14-3a3f-400f-81b5-ab0cd388d5f8',
    title: 'In Cold Blood',
    genres: 'Crime|Drama',
    director: 'Geraldine Treffrey',
    summary:
      'Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.',
    image_url: 'http://dummyimage.com/204x100.png/ff4444/ffffff',
  },
  {
    id: 'cfd56627-c342-4036-a9a3-d6661a77cd30',
    title: 'First Love',
    genres: 'Comedy|Musical',
    director: 'Pedro Benaine',
    summary:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.',
    image_url: 'http://dummyimage.com/189x100.png/ff4444/ffffff',
  },
  {
    id: 'f6be230d-a688-4e70-a083-9d9cf2f75b23',
    title: 'In Your Eyes',
    genres: 'Drama|Fantasy|Romance',
    director: 'Jordain Sagerson',
    summary:
      'Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.',
    image_url: 'http://dummyimage.com/146x100.png/5fa2dd/ffffff',
  },
  {
    id: '5a56744e-598d-4454-9fc9-dc6d677b0e79',
    title: "Rustlers' Rhapsody",
    genres: 'Comedy|Western',
    director: 'Lynnett Miall',
    summary:
      'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.',
    image_url: 'http://dummyimage.com/171x100.png/ff4444/ffffff',
  },
  {
    id: '5f7078bb-5dc9-44b0-80af-20d86e196530',
    title: 'Skyscraper (Skyskraber)',
    genres: 'Comedy|Drama',
    director: 'Kippar Collicott',
    summary:
      'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.',
    image_url: 'http://dummyimage.com/224x100.png/5fa2dd/ffffff',
  },
  {
    id: '99904f59-cbbe-46d4-81e0-f4a41e8da140',
    title: 'Air Bud',
    genres: 'Children|Comedy',
    director: 'Gasparo Warstall',
    summary:
      'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.',
    image_url: 'http://dummyimage.com/104x100.png/ff4444/ffffff',
  },
  {
    id: '8860b50d-3213-491c-847d-6c8edac7d0a6',
    title: 'In a Glass Cage (Tras el cristal)',
    genres: 'Drama|Horror',
    director: 'Silvana Collet',
    summary:
      'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',
    image_url: 'http://dummyimage.com/120x100.png/cc0000/ffffff',
  },
  {
    id: '2178510b-a96a-4be1-9dd7-f5db4c19484b',
    title: 'Citadel',
    genres: 'Drama|Horror|Thriller',
    director: 'Burl Lidden',
    summary: 'Nunc purus. Phasellus in felis.',
    image_url: 'http://dummyimage.com/105x100.png/ff4444/ffffff',
  },
  {
    id: '75fd717c-8a32-4ecd-b061-ac91ee1caeef',
    title: 'Seven Up!',
    genres: 'Documentary',
    director: 'Bruce Renols',
    summary:
      'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.',
    image_url: 'http://dummyimage.com/121x100.png/ff4444/ffffff',
  },
  {
    id: '62dc1b5f-d306-4584-a90e-bafc25affd4b',
    title: 'White Sands',
    genres: 'Drama|Thriller',
    director: 'Shayla Flude',
    summary:
      'Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.',
    image_url: 'http://dummyimage.com/199x100.png/5fa2dd/ffffff',
  },
  {
    id: '089c9533-8c4b-4371-b580-180f6d840a04',
    title: '11-11-11 (11-11-11: The Prophecy)',
    genres: 'Horror|Thriller',
    director: 'Aubrey Davidsen',
    summary:
      'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam.',
    image_url: 'http://dummyimage.com/145x100.png/dddddd/000000',
  },
  {
    id: '595b97ef-ca3c-400f-9e81-d645b545989d',
    title: 'Babar The Movie',
    genres: 'Adventure|Animation|Children|Fantasy',
    director: 'Kaela Sutworth',
    summary:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
    image_url: 'http://dummyimage.com/161x100.png/5fa2dd/ffffff',
  },
  {
    id: '68bdb1b8-2809-4675-ae54-1d5620ff111b',
    title: 'Final Cut, The',
    genres: 'Action|Drama|Thriller',
    director: 'Kippy Pointing',
    summary:
      'In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.',
    image_url: 'http://dummyimage.com/101x100.png/ff4444/ffffff',
  },
  {
    id: 'b557af77-6506-4e77-9e07-b06dacdea562',
    title: 'Serving Sara',
    genres: 'Comedy|Romance',
    director: 'Fairleigh Bendell',
    summary:
      'Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
    image_url: 'http://dummyimage.com/145x100.png/ff4444/ffffff',
  },
  {
    id: '7ebd51bb-a7d8-4d65-ada4-e7f685042daa',
    title: 'Behind Enemy Lines',
    genres: 'Action|Drama|War',
    director: 'Dalli Lead',
    summary:
      'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.',
    image_url: 'http://dummyimage.com/180x100.png/cc0000/ffffff',
  },
  {
    id: '05d39a11-c05a-4513-9c3f-155931772bc9',
    title: 'Waxwork',
    genres: 'Comedy|Horror',
    director: 'Roland Bennion',
    summary:
      'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.',
    image_url: 'http://dummyimage.com/174x100.png/cc0000/ffffff',
  },
  {
    id: 'af4f3e94-1f42-4d98-8d2d-ae58413d12ec',
    title: 'Day the Fish Came Out, The',
    genres: 'Comedy|Sci-Fi',
    director: 'Tarah Jossel',
    summary:
      'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.',
    image_url: 'http://dummyimage.com/208x100.png/ff4444/ffffff',
  },
  {
    id: '6d158f42-be51-4998-92dc-11c38e37aac7',
    title: 'Intentions of Murder (a.k.a. Murderous Instincts) (Akai satsui)',
    genres: 'Drama',
    director: 'Carola Dalgarnowch',
    summary:
      'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.',
    image_url: 'http://dummyimage.com/207x100.png/cc0000/ffffff',
  },
  {
    id: '54787a7e-5dea-4a70-885e-41dc65217667',
    title: 'I Am Ali',
    genres: 'Documentary',
    director: 'Felic Causer',
    summary:
      'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam.',
    image_url: 'http://dummyimage.com/135x100.png/5fa2dd/ffffff',
  },
  {
    id: '9ec0f6a0-09cc-43aa-8da5-0ddabcc8d92a',
    title: 'Christmas Tale, A (Un conte de Noël)',
    genres: 'Comedy|Drama',
    director: 'Torin Azam',
    summary:
      'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum.',
    image_url: 'http://dummyimage.com/114x100.png/cc0000/ffffff',
  },
  {
    id: '654659ad-291f-40f1-9907-ec27f35e93a4',
    title: "Marvin Gaye: What's Going On",
    genres: 'Documentary',
    director: 'Isidora Thorneloe',
    summary:
      'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.',
    image_url: 'http://dummyimage.com/167x100.png/ff4444/ffffff',
  },
  {
    id: 'd8bf60fe-8632-4f58-97c7-4b84be616655',
    title: 'Nazareno Cruz and the Wolf',
    genres: '(no genres listed)',
    director: 'Danya Wisby',
    summary:
      'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',
    image_url: 'http://dummyimage.com/199x100.png/dddddd/000000',
  },
  {
    id: 'd103b50a-3350-4b94-9637-8e5ebb3f9593',
    title: 'Princess Protection Program',
    genres: 'Children|Drama',
    director: 'Bruno Vanyushkin',
    summary:
      'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.',
    image_url: 'http://dummyimage.com/144x100.png/ff4444/ffffff',
  },
  {
    id: '3c91ba4c-4fe7-48b6-8ac9-10906d3d082e',
    title:
      'Lone Wolf and Cub: Sword of Vengeance (Kozure Ôkami: Kowokashi udekashi tsukamatsuru)',
    genres: 'Action|Crime',
    director: 'Giffard Vinas',
    summary:
      'Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.',
    image_url: 'http://dummyimage.com/155x100.png/dddddd/000000',
  },
  {
    id: 'c3e062e3-77b7-4f06-97f2-17dc9d014711',
    title: 'Visitor to a Museum (Posetitel muzeya)',
    genres: 'Sci-Fi',
    director: 'Granger Metrick',
    summary:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.',
    image_url: 'http://dummyimage.com/248x100.png/ff4444/ffffff',
  },
  {
    id: 'cbcd81d8-e546-4390-9bad-9c0000032ce4',
    title: 'Boss of It All, The (Direktøren for det hele)',
    genres: 'Comedy|Drama',
    director: 'Goober Uden',
    summary:
      'Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.',
    image_url: 'http://dummyimage.com/196x100.png/dddddd/000000',
  },
  {
    id: '84b348a5-0f8f-41aa-8ff9-f1d5d72cdf7b',
    title: 'Innerspace',
    genres: 'Action|Adventure|Comedy|Sci-Fi',
    director: 'Robinett Revely',
    summary:
      'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.',
    image_url: 'http://dummyimage.com/139x100.png/ff4444/ffffff',
  },
  {
    id: '06c44ceb-5766-487e-8480-f27d867155cd',
    title: "Mon oncle d'Amérique",
    genres: 'Drama',
    director: 'Noelyn Gookes',
    summary:
      'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.',
    image_url: 'http://dummyimage.com/247x100.png/5fa2dd/ffffff',
  },
  {
    id: '0ec8ae56-f238-469f-839e-978611a31040',
    title: 'Clockwork Orange, A',
    genres: 'Crime|Drama|Sci-Fi|Thriller',
    director: 'Ruperto Such',
    summary: 'Morbi quis tortor id nulla ultrices aliquet.',
    image_url: 'http://dummyimage.com/132x100.png/dddddd/000000',
  },
  {
    id: '01c9a7c8-dff9-4386-b645-43fb9e1f79cd',
    title: 'House III: The Horror Show',
    genres: 'Horror',
    director: 'Ag Leake',
    summary: 'Nulla tellus. In sagittis dui vel nisl.',
    image_url: 'http://dummyimage.com/175x100.png/ff4444/ffffff',
  },
  {
    id: 'e04e35d8-323e-442e-aa92-ea5304dc49ac',
    title: 'Valmont',
    genres: 'Drama|Romance',
    director: 'Sapphire Corroyer',
    summary:
      'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl.',
    image_url: 'http://dummyimage.com/221x100.png/cc0000/ffffff',
  },
  {
    id: '05efc148-f0cc-4672-8e7d-2c3eb43ccff5',
    title: 'Trilogy of Terror',
    genres: 'Horror|Thriller',
    director: 'Heather Irlam',
    summary:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.',
    image_url: 'http://dummyimage.com/230x100.png/5fa2dd/ffffff',
  },
  {
    id: 'ef3dc9c3-3a29-441a-b84b-7037f26623c2',
    title: 'Lemonade Joe (Limonádový Joe aneb Konská opera)',
    genres: 'Adventure|Comedy|Musical|Romance|Western',
    director: 'Carissa Kynder',
    summary:
      'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
    image_url: 'http://dummyimage.com/195x100.png/cc0000/ffffff',
  },
  {
    id: 'fead7eb3-689e-41a7-8a7a-d2c2d817e8fc',
    title: 'Delhi-6',
    genres: 'Comedy|Crime|Drama',
    director: 'Oralla Tolomelli',
    summary:
      'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.',
    image_url: 'http://dummyimage.com/247x100.png/ff4444/ffffff',
  },
  {
    id: '560c5658-25bd-4ae2-9c3f-791d98ea7d46',
    title: 'Mon Oncle Antoine',
    genres: 'Drama',
    director: 'Loren Iffe',
    summary:
      'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.',
    image_url: 'http://dummyimage.com/174x100.png/dddddd/000000',
  },
  {
    id: 'c3bd41ce-040e-44ec-9b57-f0b9872f4635',
    title: 'Once Upon a Honeymoon',
    genres: 'Comedy|Mystery|Romance',
    director: 'Tommi Zamora',
    summary:
      'In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.',
    image_url: 'http://dummyimage.com/142x100.png/ff4444/ffffff',
  },
  {
    id: '06057320-d081-4aa9-8876-f6e5cb4de030',
    title: 'Belle of the Nineties',
    genres: 'Comedy|Western',
    director: 'Fabiano Enoch',
    summary:
      'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.',
    image_url: 'http://dummyimage.com/233x100.png/dddddd/000000',
  },
  {
    id: 'e3fc6989-f379-4457-a4e7-2b9c70fc37c7',
    title: 'Rising Place, The',
    genres: 'Drama',
    director: 'Lyn Butler',
    summary: 'Suspendisse potenti. Nullam porttitor lacus at turpis.',
    image_url: 'http://dummyimage.com/196x100.png/dddddd/000000',
  },
  {
    id: '99b50e48-de14-49f6-8d51-d9a87a3c7a9c',
    title: 'Carmina or Blow Up (Carmina o revienta)',
    genres: 'Comedy|Drama',
    director: 'Marrilee Scrase',
    summary:
      'Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.',
    image_url: 'http://dummyimage.com/162x100.png/dddddd/000000',
  },
  {
    id: '700f21d8-5229-4fe4-afba-21d77ec4fe49',
    title: 'Flying Saucer, The',
    genres: 'Sci-Fi',
    director: 'Claribel McGowran',
    summary:
      'Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.',
    image_url: 'http://dummyimage.com/114x100.png/cc0000/ffffff',
  },
  {
    id: '0d76324f-a32e-4c51-bb91-db68c1e98faa',
    title: 'Night Will Fall',
    genres: 'Documentary',
    director: 'Wallas Korneev',
    summary:
      'Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.',
    image_url: 'http://dummyimage.com/131x100.png/5fa2dd/ffffff',
  },
  {
    id: 'ec8c82e7-f8ea-4f1a-9286-4eb361c5810a',
    title: 'Doppelganger',
    genres: 'Horror|Thriller',
    director: 'Coleen McMurdo',
    summary:
      'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.',
    image_url: 'http://dummyimage.com/245x100.png/cc0000/ffffff',
  },
  {
    id: '0b422152-d730-44c9-a387-005f12df6352',
    title: "God's Country",
    genres: 'Documentary',
    director: 'Winn Gillison',
    summary:
      'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.',
    image_url: 'http://dummyimage.com/240x100.png/ff4444/ffffff',
  },
  {
    id: '755d8be8-293c-41fb-b4a4-85cc2b2d9f0f',
    title: 'Bandit, The (Eskiya)',
    genres: 'Action|Crime|Romance|Thriller',
    director: 'Cynthy Kilmurry',
    summary:
      'Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla.',
    image_url: 'http://dummyimage.com/188x100.png/cc0000/ffffff',
  },
  {
    id: 'eca8c273-d9ad-448d-ad20-5d59eec72b50',
    title: 'Saturday Night',
    genres: 'Documentary',
    director: 'Edna Davis',
    summary:
      'Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum.',
    image_url: 'http://dummyimage.com/188x100.png/dddddd/000000',
  },
  {
    id: '6e5fc0d4-9ac6-431f-a08d-f50f71739644',
    title: 'Raw Deal',
    genres: 'Action',
    director: 'Daphna Diggins',
    summary:
      'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.',
    image_url: 'http://dummyimage.com/247x100.png/cc0000/ffffff',
  },
  {
    id: '5436c253-c3e9-438e-9679-74e89d2ab940',
    title: 'Chop Shop',
    genres: 'Drama',
    director: 'Vail De Domenicis',
    summary:
      'In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.',
    image_url: 'http://dummyimage.com/247x100.png/cc0000/ffffff',
  },
  {
    id: '6fec5773-f525-41cc-942d-a0c5d4247981',
    title: 'Matrimonial Comedy (Komedia malzenska)',
    genres: 'Comedy',
    director: 'Zia Liquorish',
    summary:
      'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
    image_url: 'http://dummyimage.com/230x100.png/dddddd/000000',
  },
  {
    id: 'e9481bd3-fdc6-44ed-8508-835157d74561',
    title: 'True Blue ',
    genres: 'Crime|Drama|Thriller',
    director: 'Barby Van der Spohr',
    summary:
      'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',
    image_url: 'http://dummyimage.com/129x100.png/dddddd/000000',
  },
  {
    id: 'a02d35c2-8898-4d95-bfdd-4e8a23944eee',
    title: 'Mac & Devin Go to High School',
    genres: 'Comedy',
    director: 'Marabel Haylock',
    summary:
      'Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.',
    image_url: 'http://dummyimage.com/180x100.png/dddddd/000000',
  },
  {
    id: '028de8b2-d317-47ad-8706-1dce4616cbe5',
    title: 'Curb Dance',
    genres: '(no genres listed)',
    director: 'Merl Shermore',
    summary:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.',
    image_url: 'http://dummyimage.com/157x100.png/5fa2dd/ffffff',
  },
  {
    id: 'a288e0a4-16cf-405f-b3c6-78a23f43a585',
    title: 'Lackawanna Blues',
    genres: 'Drama',
    director: 'Fidela Alessandrucci',
    summary:
      'In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.',
    image_url: 'http://dummyimage.com/177x100.png/dddddd/000000',
  },
  {
    id: '6e95534c-2839-4870-8a4b-ec37e0276339',
    title: 'Dampfnudelblues',
    genres: 'Comedy|Crime',
    director: 'Abbey Garfit',
    summary:
      'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.',
    image_url: 'http://dummyimage.com/198x100.png/5fa2dd/ffffff',
  },
  {
    id: 'd7b671e0-a696-48cd-b528-9d42f5eb7f42',
    title: 'Above Us Only Sky',
    genres: 'Drama',
    director: 'Beret Franschini',
    summary:
      'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',
    image_url: 'http://dummyimage.com/200x100.png/dddddd/000000',
  },
  {
    id: '765d3f7c-a518-4c8e-bebc-d940ff4e1405',
    title: 'Troubled Water (DeUsynlige)',
    genres: 'Drama|Thriller',
    director: 'Sergio Conrart',
    summary:
      'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl.',
    image_url: 'http://dummyimage.com/243x100.png/cc0000/ffffff',
  },
  {
    id: '7df29060-5bda-4ede-a379-597d949e0e1e',
    title: 'Passion',
    genres: 'Crime|Drama|Mystery|Thriller',
    director: 'Lockwood Dallon',
    summary:
      'Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.',
    image_url: 'http://dummyimage.com/189x100.png/5fa2dd/ffffff',
  },
  {
    id: '804e4538-c0f1-4b97-a26a-40b09baf3994',
    title: '24 Hour Party People',
    genres: 'Comedy|Drama|Musical',
    director: 'Jonie Quakley',
    summary:
      'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',
    image_url: 'http://dummyimage.com/143x100.png/cc0000/ffffff',
  },
  {
    id: '0b1a119b-cbe1-4174-8d81-ffdb02af731d',
    title: 'Back Street',
    genres: 'Drama',
    director: 'Eduardo Todarello',
    summary:
      'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.',
    image_url: 'http://dummyimage.com/123x100.png/ff4444/ffffff',
  },
  {
    id: '2c407766-8d7b-4dea-aa26-709edca59bbd',
    title: 'Recount',
    genres: 'Drama',
    director: 'Glenda Liven',
    summary:
      'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.',
    image_url: 'http://dummyimage.com/216x100.png/cc0000/ffffff',
  },
  {
    id: '45dd36d4-4f14-4633-b824-50d5997836b7',
    title: 'Alien Nation: The Enemy Within',
    genres: 'Sci-Fi',
    director: 'Berte Applebee',
    summary: 'Aenean fermentum.',
    image_url: 'http://dummyimage.com/111x100.png/cc0000/ffffff',
  },
  {
    id: '36a8538d-3859-4853-88ba-87488635cbbe',
    title: 'Free The Mind',
    genres: 'Documentary',
    director: 'Alana Whiteson',
    summary:
      'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.',
    image_url: 'http://dummyimage.com/233x100.png/cc0000/ffffff',
  },
  {
    id: 'e3d730d5-7876-46d2-9a12-934f20ba762d',
    title: 'Memphis Belle',
    genres: 'Action|Drama|War',
    director: 'Orran Dominka',
    summary:
      'Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.',
    image_url: 'http://dummyimage.com/203x100.png/ff4444/ffffff',
  },
  {
    id: '9f731124-8f8f-4c29-9999-016358ccea4a',
    title: 'Captain America',
    genres: 'Action|Fantasy|Sci-Fi|Thriller|War',
    director: 'Datha Sliman',
    summary:
      'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.',
    image_url: 'http://dummyimage.com/241x100.png/dddddd/000000',
  },
  {
    id: '03c57918-33ce-4a56-ae96-4443238d1e9b',
    title: 'Nazty Nuisance',
    genres: 'Action|Adventure|Comedy',
    director: 'Nichole Burbury',
    summary:
      'Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.',
    image_url: 'http://dummyimage.com/166x100.png/cc0000/ffffff',
  },
  {
    id: '420813c5-cd14-420d-9fa9-8ae631c8fe42',
    title: 'Ilsa, She Wolf of the SS',
    genres: 'Horror',
    director: 'Emmaline Kitchingman',
    summary:
      'In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl.',
    image_url: 'http://dummyimage.com/174x100.png/5fa2dd/ffffff',
  },
  {
    id: 'd90fa9c9-c799-47df-ba3d-80210d47a9dd',
    title: 'All About Actresses (Le bal des actrices)',
    genres: 'Comedy|Drama',
    director: 'Anna-diana Iltchev',
    summary: 'In sagittis dui vel nisl. Duis ac nibh.',
    image_url: 'http://dummyimage.com/110x100.png/ff4444/ffffff',
  },
  {
    id: '108d09fc-60c9-441e-9a85-fe9f5d484b1d',
    title: 'Keith',
    genres: 'Drama|Romance',
    director: 'Hersch Vear',
    summary:
      'Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.',
    image_url: 'http://dummyimage.com/130x100.png/ff4444/ffffff',
  },
  {
    id: '097f6827-6a8c-4e4a-8342-abf6bfaa6e15',
    title: 'Maria Bamford: The Special Special Special!',
    genres: 'Comedy',
    director: 'Sunshine Browse',
    summary:
      'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',
    image_url: 'http://dummyimage.com/152x100.png/ff4444/ffffff',
  },
  {
    id: '6101f9cf-0799-4591-bbc3-521e3d5bc1e9',
    title: 'Two: The Story of Roman & Nyro',
    genres: 'Documentary|Drama',
    director: 'Cody Norcross',
    summary:
      'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.',
    image_url: 'http://dummyimage.com/200x100.png/dddddd/000000',
  },
  {
    id: 'e5f43742-7ea6-4195-83f2-8178ffdcc867',
    title: 'Anima Mundi',
    genres: 'Documentary',
    director: 'Donna Eastment',
    summary:
      'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.',
    image_url: 'http://dummyimage.com/212x100.png/cc0000/ffffff',
  },
  {
    id: '0d87c831-ea2f-4994-af4c-1dd85f0b39b9',
    title: 'Mississippi Burning',
    genres: 'Crime|Drama|Thriller',
    director: 'Ellissa McAllaster',
    summary:
      'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.',
    image_url: 'http://dummyimage.com/118x100.png/cc0000/ffffff',
  },
  {
    id: 'b3925d9c-9462-4c61-9868-b634109c93a6',
    title: 'My Favorite Year',
    genres: 'Comedy',
    director: 'Dede Langstaff',
    summary:
      'Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero.',
    image_url: 'http://dummyimage.com/207x100.png/5fa2dd/ffffff',
  },
  {
    id: 'f7aaa4fc-8685-4cfd-b270-4a5a306d7fe6',
    title: 'Godzilla, King of the Monsters! (Kaijû-ô Gojira)',
    genres: 'Horror|Sci-Fi',
    director: 'Annemarie Vecard',
    summary:
      'Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.',
    image_url: 'http://dummyimage.com/182x100.png/5fa2dd/ffffff',
  },
  {
    id: '8cecdad9-1efd-414f-ae12-ed2ad64da7a1',
    title: 'That Certain Summer',
    genres: 'Drama',
    director: 'Eliza Wittering',
    summary:
      'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.',
    image_url: 'http://dummyimage.com/222x100.png/ff4444/ffffff',
  },
  {
    id: 'ea199029-92c7-4f6d-bd7c-0ba8d27c2d84',
    title: 'Knife in the Water (Nóz w wodzie)',
    genres: 'Drama',
    director: 'Frazer Domengue',
    summary: 'Sed sagittis.',
    image_url: 'http://dummyimage.com/212x100.png/dddddd/000000',
  },
  {
    id: '9db20b09-b32d-4cc1-b24f-3963093b3f58',
    title: 'Chaos',
    genres: 'Action|Crime|Drama|Thriller',
    director: 'Clare Janowicz',
    summary:
      'Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.',
    image_url: 'http://dummyimage.com/140x100.png/cc0000/ffffff',
  },
  {
    id: '6f2ef3b3-c4ca-49c6-9fe4-5965bca553bc',
    title: 'Lights Out',
    genres: 'Horror|Thriller',
    director: 'Karola Heads',
    summary:
      'Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.',
    image_url: 'http://dummyimage.com/188x100.png/5fa2dd/ffffff',
  },
  {
    id: '447a99b4-c618-498e-b00a-ba10d6636f23',
    title: 'Hills Have Eyes Part II, The',
    genres: 'Horror',
    director: 'Dare Barrand',
    summary:
      'Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.',
    image_url: 'http://dummyimage.com/214x100.png/ff4444/ffffff',
  },
  {
    id: '75af5d97-2c8d-4451-b6a5-a2689d3aae46',
    title: 'Public Access',
    genres: 'Drama|Thriller',
    director: 'Cass Elsop',
    summary:
      'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.',
    image_url: 'http://dummyimage.com/117x100.png/5fa2dd/ffffff',
  },
  {
    id: '092e6ba5-f17d-4d3a-b1c1-502d996b33f4',
    title: 'Ax, The (couperet, Le)',
    genres: 'Comedy|Crime',
    director: 'Englebert Stoker',
    summary:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
    image_url: 'http://dummyimage.com/139x100.png/5fa2dd/ffffff',
  },
  {
    id: '9013e33c-f484-4783-b132-8639429b9dfd',
    title: 'Sixteen Candles',
    genres: 'Comedy|Romance',
    director: 'Danni Chazerand',
    summary:
      'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
    image_url: 'http://dummyimage.com/161x100.png/ff4444/ffffff',
  },
  {
    id: '65f8706d-3337-4188-8545-af812bf13c67',
    title: "Visiteurs du soir, Les (Devil's Envoys, The)",
    genres: 'Drama|Fantasy|Romance',
    director: 'Taylor Malpas',
    summary: 'Mauris sit amet eros.',
    image_url: 'http://dummyimage.com/228x100.png/cc0000/ffffff',
  },
  {
    id: '0777959c-9dba-47b3-9741-22ee22ada67c',
    title: 'Ben',
    genres: 'Horror|Thriller',
    director: 'Gilli Delmonti',
    summary:
      'Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.',
    image_url: 'http://dummyimage.com/189x100.png/5fa2dd/ffffff',
  },
  {
    id: '66479134-a701-4726-9800-af8993da4feb',
    title: 'Straightheads (Closure)',
    genres: 'Crime|Drama',
    director: 'Rodney Sammut',
    summary:
      'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.',
    image_url: 'http://dummyimage.com/247x100.png/5fa2dd/ffffff',
  },
  {
    id: '9620a6ae-b60c-4ea3-b550-0217e3568a8f',
    title: "Sam Peckinpah's West: Legacy of a Hollywood Renegade",
    genres: 'Documentary',
    director: 'Betta Borman',
    summary:
      'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.',
    image_url: 'http://dummyimage.com/227x100.png/ff4444/ffffff',
  },
  {
    id: '48201b96-ec85-45ad-8d5d-b366ee7d0782',
    title: 'Chang: A Drama of the Wilderness',
    genres: 'Adventure|Documentary',
    director: 'Claudell Worlock',
    summary: 'Nullam sit amet turpis elementum ligula vehicula consequat.',
    image_url: 'http://dummyimage.com/209x100.png/ff4444/ffffff',
  },
  {
    id: 'f628970c-ab23-45bd-92f9-90fb4edffa92',
    title: 'Crossing Delancey',
    genres: 'Comedy|Romance',
    director: 'Allene Streatley',
    summary:
      'Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.',
    image_url: 'http://dummyimage.com/162x100.png/ff4444/ffffff',
  },
  {
    id: 'b95cba44-7fc7-46dc-b1d4-67a7755a8a9d',
    title: 'Ricky Rapper (Risto Räppääjä)',
    genres: 'Comedy|Musical',
    director: 'Winnah Vuitte',
    summary:
      'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.',
    image_url: 'http://dummyimage.com/172x100.png/5fa2dd/ffffff',
  },
  {
    id: 'cfb66388-aa13-4252-a93c-b7eda70b193b',
    title: 'Wonderful World',
    genres: 'Drama|Romance',
    director: 'Annabel Bitchener',
    summary:
      'Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.',
    image_url: 'http://dummyimage.com/175x100.png/cc0000/ffffff',
  },
  {
    id: 'f9387523-6dcf-47e0-b194-02dfa5c611e1',
    title: 'Eclipse, The',
    genres: 'Drama|Horror|Thriller',
    director: 'Demott Pawellek',
    summary:
      'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.',
    image_url: 'http://dummyimage.com/125x100.png/ff4444/ffffff',
  },
  {
    id: '13127698-b7f7-4d99-9319-df3f05dafdf3',
    title: 'Hostel: Part III ',
    genres: 'Horror|Thriller',
    director: 'Markus Dalrymple',
    summary:
      'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.',
    image_url: 'http://dummyimage.com/204x100.png/cc0000/ffffff',
  },
  {
    id: '1ff09ff0-0188-4469-8294-ee3d3934c701',
    title: 'Legacy of Rage',
    genres: 'Action',
    director: 'Lars Mahedy',
    summary: 'Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',
    image_url: 'http://dummyimage.com/160x100.png/dddddd/000000',
  },
  {
    id: 'bfc09d25-14c4-4452-a5dc-0ba602b909a9',
    title: 'Crackers',
    genres: 'Action|Comedy|Crime|Thriller',
    director: 'Wendi Keyse',
    summary:
      'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.',
    image_url: 'http://dummyimage.com/125x100.png/5fa2dd/ffffff',
  },
  {
    id: '4ba9cef4-8bff-45ba-bc7b-c14ee299054b',
    title: 'Archie To Riverdale and Back Again',
    genres: 'Comedy',
    director: 'Vivi McGlone',
    summary:
      'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.',
    image_url: 'http://dummyimage.com/184x100.png/5fa2dd/ffffff',
  },
  {
    id: '19fda1e3-d86d-4207-99b5-521a7c76f08f',
    title: 'Black Coal, Thin Ice (Bai ri yan huo)',
    genres: 'Crime|Drama|Mystery|Thriller',
    director: 'Estrellita Mounfield',
    summary:
      'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
    image_url: 'http://dummyimage.com/237x100.png/dddddd/000000',
  },
  {
    id: '9d81fde1-57a8-42aa-bee6-3851b247e269',
    title: 'Critical Care',
    genres: 'Comedy|Drama',
    director: 'Dominique Elgey',
    summary:
      'Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.',
    image_url: 'http://dummyimage.com/177x100.png/ff4444/ffffff',
  },
  {
    id: '4c3f1bce-fd13-4128-9d6b-a440125e675e',
    title: 'Edge, The (Kray)',
    genres: 'Adventure|Drama|Romance',
    director: 'Vickie McRuvie',
    summary:
      'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl.',
    image_url: 'http://dummyimage.com/218x100.png/5fa2dd/ffffff',
  },
  {
    id: 'f22eb83c-f6c0-4164-8ab8-eb06a7f126da',
    title: 'Fog, The',
    genres: 'Action|Horror|Mystery|Thriller',
    director: 'Zena Arnauduc',
    summary: 'Mauris lacinia sapien quis libero.',
    image_url: 'http://dummyimage.com/204x100.png/dddddd/000000',
  },
  {
    id: '5f55b7cc-4f9d-47a6-9d5f-f4f44120031f',
    title: 'Kevin Hart: Seriously Funny',
    genres: 'Comedy',
    director: 'Blythe Towns',
    summary: 'Sed accumsan felis.',
    image_url: 'http://dummyimage.com/139x100.png/5fa2dd/ffffff',
  },
  {
    id: '2d1e8aad-060f-4064-b708-75498323ee6c',
    title: 'Secret Reunion (Ui-hyeong-je)',
    genres: 'Drama|Thriller',
    director: 'Shantee Domleo',
    summary: 'Donec ut mauris eget massa tempor convallis.',
    image_url: 'http://dummyimage.com/160x100.png/dddddd/000000',
  },
  {
    id: '1f616285-a51f-4064-9216-7c6213fb53e6',
    title: 'Firstborn',
    genres: 'Drama|Thriller',
    director: 'Fernanda Charer',
    summary:
      'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.',
    image_url: 'http://dummyimage.com/122x100.png/ff4444/ffffff',
  },
  {
    id: '1d4f2f53-ced1-41f0-ac27-fc55543e795c',
    title: 'Encounters at the End of the World',
    genres: 'Documentary',
    director: 'Brien Seson',
    summary: 'Pellentesque viverra pede ac diam.',
    image_url: 'http://dummyimage.com/208x100.png/ff4444/ffffff',
  },
  {
    id: '7446465a-c975-4a4d-956d-01738299151c',
    title: 'American Scary',
    genres: 'Comedy|Documentary|Horror',
    director: 'Arney Davie',
    summary:
      'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.',
    image_url: 'http://dummyimage.com/166x100.png/5fa2dd/ffffff',
  },
  {
    id: '38cd61b8-3f87-41a0-b812-d55d6a523143',
    title: 'Thorn Birds, The',
    genres: 'Drama|Romance',
    director: 'Darlene Huddleston',
    summary:
      'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.',
    image_url: 'http://dummyimage.com/129x100.png/ff4444/ffffff',
  },
  {
    id: 'efc6c7ec-4f81-4c5b-830e-c0ec7597a04f',
    title: 'Mission to Mars',
    genres: 'Sci-Fi',
    director: 'Darrell Cowperthwaite',
    summary: 'Phasellus sit amet erat. Nulla tempus.',
    image_url: 'http://dummyimage.com/240x100.png/cc0000/ffffff',
  },
  {
    id: '7ab6f261-b066-43a2-8b1b-49620c17dece',
    title: 'Viva Knievel!',
    genres: 'Action',
    director: 'Livvie Minchenton',
    summary:
      'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.',
    image_url: 'http://dummyimage.com/142x100.png/ff4444/ffffff',
  },
  {
    id: 'c72beee7-abba-4d17-a2cf-e80e8eefcc76',
    title: 'Swamp Shark',
    genres: 'Sci-Fi',
    director: 'Bidget Eliasen',
    summary:
      'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam.',
    image_url: 'http://dummyimage.com/198x100.png/dddddd/000000',
  },
  {
    id: 'e5020452-83e3-4d91-91c9-07720dd1c411',
    title: 'Police Academy: Mission to Moscow',
    genres: 'Comedy|Crime',
    director: 'Claiborne Kavanagh',
    summary:
      'In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.',
    image_url: 'http://dummyimage.com/201x100.png/dddddd/000000',
  },
  {
    id: 'e459533a-fe09-480e-b2a4-c0aee40bab41',
    title: 'Haunting, The',
    genres: 'Horror|Thriller',
    director: 'Jens Burrage',
    summary:
      'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
    image_url: 'http://dummyimage.com/142x100.png/5fa2dd/ffffff',
  },
  {
    id: 'cd34218d-38d9-4e41-91b0-331c90469123',
    title: 'First Name: Carmen (Prénom Carmen)',
    genres: 'Crime|Drama|Romance',
    director: 'Nata Rhule',
    summary:
      'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.',
    image_url: 'http://dummyimage.com/209x100.png/cc0000/ffffff',
  },
  {
    id: '2e20a993-6800-4ce7-b655-4cd418762b31',
    title: 'Quiet City',
    genres: 'Drama',
    director: 'Judah Andresen',
    summary:
      'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.',
    image_url: 'http://dummyimage.com/162x100.png/5fa2dd/ffffff',
  },
  {
    id: 'b2e886da-b941-49b9-97d9-b8e8bf1036e0',
    title: 'Like Mike 2: Streetball',
    genres: 'Children|Comedy',
    director: 'Celestia Haining',
    summary:
      'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
    image_url: 'http://dummyimage.com/205x100.png/ff4444/ffffff',
  },
  {
    id: '94cfc534-7465-4f3b-b9c2-f5bebf06f0c7',
    title: 'Zed & Two Noughts, A',
    genres: 'Drama',
    director: 'Mar Carleman',
    summary:
      'Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',
    image_url: 'http://dummyimage.com/246x100.png/dddddd/000000',
  },
  {
    id: 'c8c346e8-279d-4e19-8ed1-68b5c2ea03dd',
    title: 'Snake of June, A (Rokugatsu no hebi)',
    genres: 'Drama|Mystery',
    director: 'Helen Geck',
    summary:
      'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum.',
    image_url: 'http://dummyimage.com/212x100.png/dddddd/000000',
  },
  {
    id: '8028eafa-ca7f-48e1-a033-28d94e19e820',
    title: 'Paradise Now',
    genres: 'Crime|Drama|Thriller|War',
    director: 'Ambur Aulds',
    summary:
      'Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',
    image_url: 'http://dummyimage.com/158x100.png/ff4444/ffffff',
  },
  {
    id: '819bf83c-5eab-4019-97a5-827bb7d05bbc',
    title: 'Masked & Anonymous',
    genres: 'Comedy|Drama',
    director: 'Kerstin Hadny',
    summary:
      'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.',
    image_url: 'http://dummyimage.com/131x100.png/ff4444/ffffff',
  },
  {
    id: 'add9412e-945d-4698-b614-6c989baf3e79',
    title: 'Godzilla, King of the Monsters! (Kaijû-ô Gojira)',
    genres: 'Horror|Sci-Fi',
    director: 'Astrid Housecraft',
    summary:
      'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.',
    image_url: 'http://dummyimage.com/177x100.png/cc0000/ffffff',
  },
  {
    id: '811abdd8-529a-40a2-922a-163d7a19aaa5',
    title: 'Get on the Bus',
    genres: 'Drama',
    director: 'Chalmers Winnard',
    summary:
      'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.',
    image_url: 'http://dummyimage.com/184x100.png/ff4444/ffffff',
  },
  {
    id: 'cfae535d-9f8f-4d46-84f4-b9d8b5fc90bd',
    title: 'George and the Dragon',
    genres: 'Action|Adventure|Children|Comedy|Fantasy',
    director: 'Filmer Shulver',
    summary:
      'Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.',
    image_url: 'http://dummyimage.com/209x100.png/cc0000/ffffff',
  },
  {
    id: 'a03dec74-9891-44da-a9a8-7ea1c448f8a2',
    title: "Breakin' All the Rules",
    genres: 'Comedy|Romance',
    director: 'Zena McIlmorow',
    summary:
      'Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.',
    image_url: 'http://dummyimage.com/134x100.png/ff4444/ffffff',
  },
  {
    id: '83f21974-82ae-4bd1-a4ce-dd81f4cb7cea',
    title: 'Redline',
    genres: 'Action',
    director: 'Benjie Brownill',
    summary:
      'Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.',
    image_url: 'http://dummyimage.com/216x100.png/dddddd/000000',
  },
  {
    id: 'f80969ea-be9a-47cb-a9e6-37c09e3ae118',
    title: 'Soupe aux Choux, La',
    genres: 'Comedy',
    director: 'Shep Hunstone',
    summary:
      'Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.',
    image_url: 'http://dummyimage.com/147x100.png/ff4444/ffffff',
  },
  {
    id: '87398c18-d28a-43d8-b1a0-8a2e3e89b64d',
    title: 'Women of the Night (Yoru no onnatachi)',
    genres: 'Drama',
    director: 'Cicily Wackett',
    summary:
      'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui.',
    image_url: 'http://dummyimage.com/150x100.png/5fa2dd/ffffff',
  },
  {
    id: '7db16f54-d5a9-4ea9-acc2-5c554f815ca3',
    title: 'Portraits Chinois',
    genres: 'Drama',
    director: 'Katha Masser',
    summary:
      'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo.',
    image_url: 'http://dummyimage.com/175x100.png/dddddd/000000',
  },
  {
    id: '2a77d521-3156-4c67-93f7-50c0592ec815',
    title: "Vive L'Amour (Ai qing wan sui)",
    genres: 'Drama',
    director: 'Rawley Carine',
    summary: 'Fusce consequat. Nulla nisl. Nunc nisl.',
    image_url: 'http://dummyimage.com/166x100.png/ff4444/ffffff',
  },
  {
    id: '6f344f6f-e0b2-430d-9480-9c7ccd047fc5',
    title: "Last Man on Earth, The (Ultimo uomo della Terra, L')",
    genres: 'Drama|Horror|Sci-Fi',
    director: 'Beverie Ferriby',
    summary:
      'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
    image_url: 'http://dummyimage.com/221x100.png/5fa2dd/ffffff',
  },
  {
    id: 'ee05513d-74c6-433c-ac07-5648ca6df4dc',
    title: 'Down by Law',
    genres: 'Comedy|Drama|Film-Noir',
    director: 'Agnesse Shout',
    summary:
      'Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.',
    image_url: 'http://dummyimage.com/210x100.png/cc0000/ffffff',
  },
  {
    id: '347ae206-9ae5-4056-83d4-c2d1a6cbe30c',
    title: "I Don't Know How She Does It",
    genres: 'Comedy',
    director: 'Bone Costa',
    summary:
      'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.',
    image_url: 'http://dummyimage.com/101x100.png/ff4444/ffffff',
  },
  {
    id: 'adcd224b-e825-4e54-a881-7c7e01b41222',
    title: '100 Ways to Murder Your Wife (Sha qi er ren zu)',
    genres: 'Comedy',
    director: 'Turner Scad',
    summary:
      'Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.',
    image_url: 'http://dummyimage.com/250x100.png/5fa2dd/ffffff',
  },
  {
    id: 'ce3323e6-3378-46ce-9ad7-7f46356d9b64',
    title: 'Mouchette',
    genres: 'Drama',
    director: 'Delaney Barnes',
    summary:
      'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.',
    image_url: 'http://dummyimage.com/165x100.png/cc0000/ffffff',
  },
  {
    id: '9a054357-5ef1-4409-8990-904438c03ef0',
    title: 'Highlander II: The Quickening',
    genres: 'Action|Sci-Fi',
    director: 'Isaak Whitfeld',
    summary:
      'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante.',
    image_url: 'http://dummyimage.com/169x100.png/5fa2dd/ffffff',
  },
  {
    id: '172126db-c3ed-4e09-90c5-139475754cfb',
    title: 'Tae Guk Gi: The Brotherhood of War (Taegukgi hwinalrimyeo)',
    genres: 'Action|Drama|War',
    director: 'Jakie Splevin',
    summary:
      'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.',
    image_url: 'http://dummyimage.com/123x100.png/dddddd/000000',
  },
  {
    id: '24571bd8-a258-477d-93d4-064f9b1048d0',
    title: 'Trans-Europ-Express',
    genres: 'Thriller',
    director: 'Roth Mulrenan',
    summary:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.',
    image_url: 'http://dummyimage.com/159x100.png/ff4444/ffffff',
  },
  {
    id: '437bbeab-8b4c-49fa-af08-4430b190a166',
    title: 'Brøken, The (a.k.a. Broken, The)',
    genres: 'Drama|Horror|Thriller',
    director: 'Chelsea Finden',
    summary:
      'Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',
    image_url: 'http://dummyimage.com/133x100.png/ff4444/ffffff',
  },
  {
    id: 'af522dff-8ed8-4ae2-b254-7c3b7a5d5b17',
    title: 'Last Call, The (Tercera Llamada)',
    genres: 'Comedy|Drama',
    director: 'Mort Waller',
    summary:
      'Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.',
    image_url: 'http://dummyimage.com/250x100.png/cc0000/ffffff',
  },
  {
    id: '4bbbb5cc-0bee-4352-bef9-be339cba41f7',
    title: 'Dreams (Kvinnodröm)',
    genres: 'Drama',
    director: 'Zia Risso',
    summary:
      'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.',
    image_url: 'http://dummyimage.com/112x100.png/ff4444/ffffff',
  },
  {
    id: 'a5f7bb3f-6b65-4571-817c-ce49e1cbab10',
    title: 'Sexy Nights of the Living Dead',
    genres: 'Horror',
    director: 'Laurella Dare',
    summary:
      'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla.',
    image_url: 'http://dummyimage.com/172x100.png/dddddd/000000',
  },
  {
    id: '399c242e-77ae-4002-9126-5a1ad60fa085',
    title: 'History Is Made at Night',
    genres: 'Drama|Romance',
    director: 'Camella Mulkerrins',
    summary:
      'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.',
    image_url: 'http://dummyimage.com/121x100.png/dddddd/000000',
  },
  {
    id: 'bf8aca80-4ea0-47ca-a42b-489fc5c9acd3',
    title: 'Bad Boy (Dawg)',
    genres: 'Comedy|Drama|Romance',
    director: 'Konrad Worwood',
    summary:
      'Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.',
    image_url: 'http://dummyimage.com/166x100.png/dddddd/000000',
  },
  {
    id: '15083873-c976-4856-b262-bbfc31e9bacb',
    title: 'Girl on a Bicycle',
    genres: 'Comedy|Romance',
    director: 'Korie Dennehy',
    summary:
      'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.',
    image_url: 'http://dummyimage.com/160x100.png/dddddd/000000',
  },
  {
    id: '3bf9b681-00f4-434c-a470-e307b6d2dee1',
    title: 'China Lake Murders, The',
    genres: 'Drama|Thriller',
    director: 'Florette Kenneway',
    summary:
      'Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.',
    image_url: 'http://dummyimage.com/142x100.png/5fa2dd/ffffff',
  },
  {
    id: '1cffb539-0de5-4d8a-b09f-9eca5553c413',
    title: 'I, Monster',
    genres: 'Horror|Sci-Fi',
    director: 'Shaylah Dodle',
    summary:
      'Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.',
    image_url: 'http://dummyimage.com/174x100.png/cc0000/ffffff',
  },
  {
    id: 'dab485e0-1fe0-428e-9e8d-1fad3f59e8e3',
    title: "Operation 'Y' & Other Shurik's Adventures",
    genres: 'Comedy|Crime|Romance',
    director: 'Katharine Curnok',
    summary:
      'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.',
    image_url: 'http://dummyimage.com/220x100.png/5fa2dd/ffffff',
  },
  {
    id: '139428e0-55c7-4574-af51-6c0f0b631b46',
    title: 'Confessor Caressor',
    genres: '(no genres listed)',
    director: 'Ephrayim Davidovicz',
    summary:
      'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna.',
    image_url: 'http://dummyimage.com/109x100.png/ff4444/ffffff',
  },
  {
    id: 'bf8529fd-59c3-4355-a314-9748e701d046',
    title: 'Ewok Adventure, The (a.k.a. Caravan of Courage: An Ewok Adventure)',
    genres: 'Adventure|Children|Fantasy|Sci-Fi',
    director: 'Ragnar Couve',
    summary:
      'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.',
    image_url: 'http://dummyimage.com/218x100.png/dddddd/000000',
  },
  {
    id: '50930236-d5ed-4f3e-83b0-3d8bd54d1585',
    title: 'Birth',
    genres: 'Drama|Mystery',
    director: 'Roseanne Spaunton',
    summary: 'Cras in purus eu magna vulputate luctus.',
    image_url: 'http://dummyimage.com/246x100.png/ff4444/ffffff',
  },
  {
    id: 'e4509da0-6c2d-4f15-86cb-f125e240f17d',
    title: 'Fast & Furious 6 (Fast and the Furious 6, The)',
    genres: 'Action|Crime|Thriller|IMAX',
    director: 'Vi Blissitt',
    summary:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.',
    image_url: 'http://dummyimage.com/109x100.png/ff4444/ffffff',
  },
  {
    id: 'c68e8255-f956-49a9-b1cb-78cfe1c1a4d0',
    title: "I Am Love (Io sono l'amore)",
    genres: 'Drama|Romance',
    director: 'Broddy Shakspeare',
    summary:
      'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo.',
    image_url: 'http://dummyimage.com/135x100.png/cc0000/ffffff',
  },
  {
    id: '5e2d9aa1-587d-4b54-a5e0-35ea82e31e7d',
    title: "Picnic on the Grass (Le déjeuner sur l'herbe)",
    genres: 'Comedy|Romance',
    director: 'Edgardo Startin',
    summary:
      'Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.',
    image_url: 'http://dummyimage.com/126x100.png/ff4444/ffffff',
  },
  {
    id: '99c1e6d0-cca1-49c0-8ddd-7ac5905c53b3',
    title: 'Young Doctors in Love',
    genres: 'Comedy',
    director: 'Selena Berringer',
    summary:
      'Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.',
    image_url: 'http://dummyimage.com/115x100.png/cc0000/ffffff',
  },
  {
    id: 'f5e8edd7-2eb8-401d-863d-e9083154b838',
    title: 'Shark Attack',
    genres: 'Action|Horror',
    director: 'Karrah Taks',
    summary:
      'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.',
    image_url: 'http://dummyimage.com/248x100.png/5fa2dd/ffffff',
  },
  {
    id: '2a16ea92-b428-45c1-84c4-c2acb02bc583',
    title: 'Float',
    genres: 'Drama|Romance',
    director: 'Innis Southcott',
    summary:
      'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla.',
    image_url: 'http://dummyimage.com/234x100.png/5fa2dd/ffffff',
  },
  {
    id: 'e2c8b6f3-1710-4cf5-a68b-9e757c9c0778',
    title: 'White Countess, The',
    genres: 'Drama',
    director: 'Caldwell Iacofo',
    summary:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.',
    image_url: 'http://dummyimage.com/211x100.png/5fa2dd/ffffff',
  },
  {
    id: '1e78da03-3e08-41f8-88f0-161af89fa51d',
    title: 'Hall Pass',
    genres: 'Comedy',
    director: 'Ravid Hiland',
    summary:
      'Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.',
    image_url: 'http://dummyimage.com/235x100.png/dddddd/000000',
  },
  {
    id: 'e9781970-ccc7-4c73-ae38-fd0d43dfd533',
    title: 'Gun the Man Down',
    genres: 'Western',
    director: 'Caryl Gillingham',
    summary:
      'Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.',
    image_url: 'http://dummyimage.com/224x100.png/5fa2dd/ffffff',
  },
  {
    id: 'dfd03c0f-84e4-419d-b738-ad48a5c5ab38',
    title: 'Flying Swords of Dragon Gate, The (Long men fei jia)',
    genres: 'Action|Adventure|IMAX',
    director: 'Lusa Laughlin',
    summary:
      'Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.',
    image_url: 'http://dummyimage.com/114x100.png/cc0000/ffffff',
  },
  {
    id: 'a9f033bd-d017-443f-84d6-19515bfb71ec',
    title: 'MVP: Most Valuable Primate',
    genres: 'Comedy',
    director: 'Amelina Dyer',
    summary:
      'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.',
    image_url: 'http://dummyimage.com/102x100.png/cc0000/ffffff',
  },
  {
    id: 'e91d3c7c-c0c7-4b7d-ad5b-c9b0e67c5284',
    title: 'I Know What You Did Last Summer',
    genres: 'Horror|Mystery|Thriller',
    director: 'Chaunce Gasnoll',
    summary:
      'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.',
    image_url: 'http://dummyimage.com/140x100.png/ff4444/ffffff',
  },
  {
    id: '49293260-9fea-4acb-a353-63fb5eb0cec2',
    title: 'Paradise Road',
    genres: 'Drama|War',
    director: 'Eugenio Lammie',
    summary:
      'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque.',
    image_url: 'http://dummyimage.com/108x100.png/ff4444/ffffff',
  },
  {
    id: '70c77306-7cda-4416-a829-6853ab14f6b0',
    title: 'Murder by Death',
    genres: 'Comedy|Crime|Mystery|Thriller',
    director: 'Timmy Byrnes',
    summary:
      'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.',
    image_url: 'http://dummyimage.com/153x100.png/5fa2dd/ffffff',
  },
  {
    id: '6d1e65ed-d47b-4a9b-af91-c52e2d4c3662',
    title: 'Prince and the Showgirl, The',
    genres: 'Comedy|Romance',
    director: 'Suki Harmon',
    summary: 'In congue.',
    image_url: 'http://dummyimage.com/132x100.png/5fa2dd/ffffff',
  },
  {
    id: '98febb38-3b24-4fa3-9bbc-f86a154aa0a3',
    title: 'Standing in the Shadows of Motown',
    genres: 'Documentary|Musical',
    director: 'Pepillo Hawarden',
    summary:
      'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.',
    image_url: 'http://dummyimage.com/232x100.png/5fa2dd/ffffff',
  },
  {
    id: '86fd8c42-ef0a-45b4-819b-6e16a3715989',
    title: 'Nugget, The',
    genres: 'Comedy',
    director: 'Gay Darnody',
    summary:
      'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus.',
    image_url: 'http://dummyimage.com/133x100.png/5fa2dd/ffffff',
  },
  {
    id: '7e0f94ef-3658-4e7f-99d1-dbd8bd5285ec',
    title: 'Billy Jack',
    genres: 'Action|Drama',
    director: 'Clarissa Cleatherow',
    summary:
      'Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.',
    image_url: 'http://dummyimage.com/217x100.png/5fa2dd/ffffff',
  },
  {
    id: '2de120b1-f0ac-47bf-98b4-e87511f32c84',
    title: "Gunnin' for That #1 Spot",
    genres: 'Documentary',
    director: 'Loralyn Fysh',
    summary: 'Morbi quis tortor id nulla ultrices aliquet.',
    image_url: 'http://dummyimage.com/135x100.png/5fa2dd/ffffff',
  },
  {
    id: '0f758017-fc5b-4db1-be35-e26b79a514d2',
    title: 'Only Angels Have Wings',
    genres: 'Adventure|Drama|Romance',
    director: 'Brewer Gabel',
    summary:
      'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.',
    image_url: 'http://dummyimage.com/182x100.png/dddddd/000000',
  },
  {
    id: '8593b827-8619-4a87-805d-86f92732bad4',
    title: "Côte d'Azur (Crustacés et coquillages)",
    genres: 'Comedy|Musical|Romance',
    director: 'Griffie Laws',
    summary:
      'In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.',
    image_url: 'http://dummyimage.com/230x100.png/ff4444/ffffff',
  },
  {
    id: 'f8f42cbb-3ca4-4fcf-80c8-cdc822231d79',
    title: 'Pittsburgh',
    genres: 'Drama',
    director: 'Thane Bramford',
    summary:
      'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.',
    image_url: 'http://dummyimage.com/123x100.png/ff4444/ffffff',
  },
  {
    id: '894565b0-8f84-4a3d-af56-fbda75af2776',
    title: 'Julia and Julia (Giulia e Giulia)',
    genres: 'Drama|Mystery|Thriller',
    director: 'Jsandye Bees',
    summary:
      'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla.',
    image_url: 'http://dummyimage.com/144x100.png/dddddd/000000',
  },
  {
    id: '6a05cd21-06a0-43e9-a068-6aef43bf4f4a',
    title: 'Spanglish',
    genres: 'Comedy|Drama|Romance',
    director: 'Jordain Sobtka',
    summary:
      'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.',
    image_url: 'http://dummyimage.com/210x100.png/dddddd/000000',
  },
  {
    id: '1e41be3f-c1d0-4f88-9af2-6df2d9eb6287',
    title: 'Long Hello and Short Goodbye',
    genres: 'Comedy|Thriller',
    director: 'Trace Plank',
    summary:
      'Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.',
    image_url: 'http://dummyimage.com/240x100.png/ff4444/ffffff',
  },
];
