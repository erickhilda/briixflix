import { defineStore } from 'pinia';
import { movies as movieList } from './movies-mock';

export type Movie = {
  id: string;
  title: string;
  director: string;
  summary: string;
  genres: string;
  image_url: string;
};

const useMovieStore = defineStore('movie', {
  state: () => ({
    movies: movieList as Movie[],
    movie: null as Movie | null | undefined,
  }),
  actions: {
    addMovie(movie: Movie) {
      return new Promise((resolve) => {
        setTimeout(() => {
          this.movies.unshift(movie);
          resolve(true);
        }, 1000);
      });
    },
    editMovie(movie: Movie) {
      return new Promise((resolve) => {
        setTimeout(() => {
          const index = this.movies.findIndex((m) => m.id === movie.id);
          if (index === -1) {
            resolve(false);
            return;
          }
          this.movies.splice(index, 1);
          this.movies.unshift(movie);
          resolve(true);
        }, 1000);
      });
    },
    getMovie(id: string) {
      return new Promise((resolve) => {
        setTimeout(() => {
          this.movie = this.movies.find((m) => m.id === id);
          resolve(true);
        }, 1000);
      });
    },
    deleteMovie(id: string) {
      return new Promise((resolve) => {
        setTimeout(() => {
          const index = this.movies.findIndex((m) => m.id === id);
          this.movies.splice(index, 1);
          resolve(true);
        }, 1000);
      });
    },
  },
});

export default useMovieStore;
