import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: 'movie/add', component: () => import('pages/AddMoviePage.vue') },
      // { path: 'movie/:id', component: () => import('pages/MoviePage.vue') },
      {
        path: 'movie/:id/edit',
        component: () => import('pages/EditMoviePage.vue'),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
